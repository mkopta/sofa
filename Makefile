all:
	cd src/ && make && cp sofa .. && make clean

clean:
	cd src/ && make clean
	rm -f sofa

install:
	cd src/ && make install

uninstall:
	cd src/ && make uninstall
