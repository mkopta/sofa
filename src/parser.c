#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "./lexan.h"
#include "./list.h"
#include "./parser.h"

static int token;

static int ident_compare(const void *a, const void *b);
static int trans_compare(const void *a, const void *b);
static struct ast_node_alphabet parser_parse_alphabet(void);
static struct ast_node_states parser_parse_states(const struct list *alphabet);
static struct ast_node_init parser_parse_init(const struct list *states);
static struct ast_node_final parser_parse_final(const struct list *states);
static struct ast_node_delta parser_parse_delta(const struct list *alphabet,
    const struct list *states);
static struct list *parser_parse_identifiers(void);
static void parser_parse_identifiers2(struct list *identifiers);
static struct list *parser_parse_transitions(const struct list *alphabet, const
    struct list *states);
static void parser_parse_transitions2(const struct list *alphabet, const struct
    list *states, struct list *transitions);
static identifier parser_parse_identifier(void);
static struct ast_node_transition *parser_parse_transition(const struct list
    *alphabet, const struct list *states);
static void check_alphabet(const struct ast_node_alphabet *alphabet);
static void check_states(const struct list *alphabet, const struct
    ast_node_states *states);
static void check_init(const struct list *states, const struct ast_node_init
    *init);
static void check_final(const struct list *states, const struct ast_node_final
    *final);
static void check_transitions(const struct list *transitions);
static void check_transition(const struct list *alphabet, const struct list
    *states, const struct ast_node_transition *transition);


static int ident_compare(const void *a, const void *b) {
  return strcmp((char *) a, (char *) b);
}

/* FIXME - UGLY */
static int trans_compare(const void *a, const void *b) {
  int result = 0;
  struct list_node *n = NULL;
  struct ast_node_transition *trans_a = (struct ast_node_transition *) a;
  struct ast_node_transition *trans_b = (struct ast_node_transition *) b;
  if ((result = strcmp(trans_a->from, trans_b->from)) != 0)
    return result;
  if (trans_a->via == NULL && trans_b->via != NULL)
    return 1;
  if (trans_a->via != NULL && trans_b->via == NULL)
    return -1;
  if (trans_a->via != NULL && trans_b->via != NULL)
    if ((result = strcmp(trans_a->via, trans_b->via)) != 0)
      return result;
  n = trans_b->to->head;
  while (n != NULL) {
    if (!list_contains(trans_a->to, n->data, ident_compare))
      return 1;
    n = n->next;
  }
  n = trans_a->to->head;
  while (n != NULL) {
    if (!list_contains(trans_b->to, n->data, ident_compare))
      return -1;
    n = n->next;
  }
  return result;
}

void parser_init(FILE *fp) {
  lex_init(fp);
}

struct ast_node_automaton parser_parse_automaton(void) {
  struct ast_node_automaton automaton;
  automaton.alphabet = parser_parse_alphabet();
  automaton.states = parser_parse_states(automaton.alphabet.symbols);
  automaton.init = parser_parse_init(automaton.states.states);
  automaton.final = parser_parse_final(automaton.states.states);
  automaton.delta = parser_parse_delta(automaton.alphabet.symbols,
      automaton.states.states);
  token = lex_get_next_token();
  if (token != LEX_EOF) {
    fprintf(stderr, "Syntax error. Instead of end of file,");
    fprintf(stderr, " garbage follows.\n");
    exit(2);
  }
  return automaton;
}

static struct ast_node_alphabet parser_parse_alphabet(void) {
  struct ast_node_alphabet alphabet;
  token = lex_get_next_token();
  if (token != LEX_ALPHABET) {
    fprintf(stderr, "Syntax error while parsing alphabet. ");
    fprintf(stderr, "Missing 'alphabet' keyword.\n");
    exit(2);
  }
  alphabet.symbols = parser_parse_identifiers();
  check_alphabet(&alphabet);
  return alphabet;
}

static struct ast_node_states parser_parse_states(const struct list *alphabet) {
  struct ast_node_states states;
  token = lex_get_next_token();
  if (token != LEX_STATES) {
    fprintf(stderr, "Syntax error while parsing states. ");
    fprintf(stderr, "Missing 'states' keyword.\n");
    exit(2);
  }
  states.states = parser_parse_identifiers();
  check_states(alphabet, &states);
  return states;
}

static struct ast_node_init parser_parse_init(const struct list *states) {
  struct ast_node_init init;
  token = lex_get_next_token();
  if (token != LEX_INIT) {
    fprintf(stderr, "Syntax error while parsing init. ");
    fprintf(stderr, "Missing 'init' keyword.\n");
    exit(2);
  }
  init.states = parser_parse_identifiers();
  check_init(states, &init);
  return init;
}

static struct ast_node_final parser_parse_final(const struct list *states) {
  struct ast_node_final final;
  token = lex_get_next_token();
  if (token != LEX_FINAL) {
    fprintf(stderr, "Syntax error while parsing final. ");
    fprintf(stderr, "Missing 'final' keyword.\n");
    exit(2);
  }
  final.states = parser_parse_identifiers();
  check_final(states, &final);
  return final;
}

static struct ast_node_delta parser_parse_delta(const struct list *alphabet, const
    struct list *states) {
  struct ast_node_delta delta;
  token = lex_get_next_token();
  if (token != LEX_DELTA) {
    fprintf(stderr, "Syntax error while parsing delta. ");
    fprintf(stderr, "Missing 'delta' keyword.\n");
    exit(2);
  }
  delta.transitions = parser_parse_transitions(alphabet, states);
  return delta;
}

static struct list *parser_parse_identifiers(void) {
  struct list *identifiers = list_create();
  token = lex_get_next_token();
  if (token != LEX_LCB) {
    fprintf(stderr, "Syntax error while parsing identifiers. ");
    fprintf(stderr, "Missing '{' (left curly bracket).\n");
    exit(2);
  }
  parser_parse_identifiers2(identifiers);
  if (token != LEX_RCB) {
    fprintf(stderr, "Syntax error while parsing identifiers. ");
    fprintf(stderr, "Missing '}' (right curly bracket).\n");
    exit(2);
  }
  return identifiers;
}

static void parser_parse_identifiers2(struct list *l) {
  token = lex_get_next_token();
  switch (token) {
    case LEX_IDENT:
      list_push(l, (void *) parser_parse_identifier());
      parser_parse_identifiers2(l);
      break;
    case LEX_RCB:
      break;
    default:
      fprintf(stderr, "Syntax error while parsing identifiers. ");
      fprintf(stderr, "Expecting '}' (right curly bracket) or ");
      fprintf(stderr, "next identifier.\n");
      exit(2);
  }
}


static struct list *parser_parse_transitions(const struct list *alphabet, const struct
    list *states) {
  struct list *transitions = list_create();
  token = lex_get_next_token();
  if (token != LEX_LCB) {
    fprintf(stderr, "Syntax error while parsing transitions. ");
    fprintf(stderr, "Missing '{' (left curly bracket).\n");
    exit(2);
  }
  parser_parse_transitions2(alphabet, states, transitions);
  if (token != LEX_RCB) {
    fprintf(stderr, "Syntax error while parsing transitions. ");
    fprintf(stderr, "Missing '}' (right curly bracket).\n");
    exit(2);
  }
  check_transitions(transitions);
  return transitions;
}

static void parser_parse_transitions2(const struct list *alphabet, const struct list
    *states, struct list *transitions) {
  token = lex_get_next_token();
  switch (token) {
    case LEX_IDENT:
      list_push(transitions,
          (void *) parser_parse_transition(alphabet, states));
      parser_parse_transitions2(alphabet, states, transitions);
      break;
    case LEX_RCB:
      break;
    default:
      fprintf(stderr, "Syntax error while parsing transitions. ");
      fprintf(stderr, "Expecting '}' (right curly bracket) or ");
      fprintf(stderr, "next transition.\n");
      exit(2);
  }
}

static identifier parser_parse_identifier(void) {
  char *ident = NULL;
  size_t ident_size = strlen(lex_ident);
  unsigned i;
  assert(token == LEX_IDENT);
  ident = (char *) malloc(sizeof(char) * ident_size);
  if (ident == NULL) {
    perror("malloc");
    exit(2);
  }
  for (i = 0; i < ident_size; i++)
    ident[i] = lex_ident[i];
  return ident;
}

static struct ast_node_transition *parser_parse_transition(const struct list *alphabet,
    const struct list *states) {
  struct ast_node_transition *t = (struct ast_node_transition *)
    malloc(sizeof(struct ast_node_transition));
  if (t == NULL) {
    perror("malloc");
    exit(2);
  }
  assert(token == LEX_IDENT);
  t->from = parser_parse_identifier();
  token = lex_get_next_token();
  if (token != LEX_LBB) {
    fprintf(stderr, "Syntax error while parsing transition. ");
    fprintf(stderr, "Missing '[' (left box bracket).\n");
    exit(2);
  }
  token = lex_get_next_token();
  if (token != LEX_IDENT && token != LEX_RBB) {
    fprintf(stderr, "Syntax error while parsing transition. ");
    fprintf(stderr, "Expecting identifier between [  ] or ");
    fprintf(stderr, "closing ']' (right box bracket).\n");
    exit(2);
  }
  if (token != LEX_RBB) {
    t->via = parser_parse_identifier();
    token = lex_get_next_token();
    if (token != LEX_RBB) {
      fprintf(stderr, "Syntax error while parsing transition. ");
      fprintf(stderr, "Missing ']' (right box bracket).\n");
      exit(2);
    }
  } else {
    t->via = NULL;
  }
  token = lex_get_next_token();
  if (token != LEX_ARROW) {
    fprintf(stderr, "Syntax error while parsing transition. ");
    fprintf(stderr, "Missing '->' (arrow).\n");
    exit(2);
  }
  t->to = parser_parse_identifiers();
  check_transition(alphabet, states, t);
  return t;
}

static void check_alphabet(const struct ast_node_alphabet *alphabet) {
  struct list_node *n = NULL;
  if ((n = list_find_first_duplicate(alphabet->symbols, ident_compare)) != NULL) {
    fprintf(stderr, "Semantic error. ");
    fprintf(stderr, "Identifier '%s' in 'alphabet' defined more than once.\n",
        (char *) n->data);
    exit(2);
  }
}

static void check_states(const struct list *alphabet, const struct
    ast_node_states *states) {
  struct list_node *n = NULL;
  if ((n = list_find_first_duplicate(states->states, ident_compare)) != NULL) {
    fprintf(stderr, "Semantic error. ");
    fprintf(stderr, "Identifier '%s' in 'states' defined more than once.\n",
        (char *) n->data);
    exit(2);
  }
  /* Actualy, there is no need to 'alphabet' and 'states' be disjoint

  if ((n = list_find_first_common(states->states, alphabet, ident_compare)) != NULL) {
    fprintf(stderr, "Semantic error. ");
    fprintf(stderr, "Identifier '%s' defined both in 'states' and 'alphabet'. ",
        (char *) n->data);
    fprintf(stderr, "States and alphabet must be disjoint\n");
    exit(2);
  }
  */
}

static void check_init(const struct list *states, const struct ast_node_init
    *init) {
  struct list_node *n;
  if ((n = list_find_first_duplicate(init->states, ident_compare)) != NULL) {
    fprintf(stderr, "Semantic error. ");
    fprintf(stderr, "Identifier '%s' in 'init' defined more than once.\n",
        (char *) n->data);
    exit(2);
  }
  n = init->states->head;
  while (n != NULL) {
    if (!list_contains(states, n->data, ident_compare)) {
      fprintf(stderr, "Semantic error. ");
      fprintf(stderr, "State '%s' in 'init' isn't contained in 'states'.\n",
          (char *) n->data);
      exit(2);
    }
    n = n->next;
  }
  if (init->states->size == 0) {
    fprintf(stderr, "Semantic error. ");
    fprintf(stderr, "Section 'init' cannot be empty.\n");
    exit(2);
  }
}

static void check_final(const struct list *states, const struct ast_node_final
    *final) {
  struct list_node *n = NULL;
  if ((n = list_find_first_duplicate(final->states, ident_compare)) != NULL) {
    fprintf(stderr, "Semantic error. ");
    fprintf(stderr, "Identifier '%s' in 'final' defined more than once.\n",
        (char *) n->data);
    exit(2);
  }
  n = final->states->head;
  while (n != NULL) {
    if (!list_contains(states, n->data, ident_compare)) {
      fprintf(stderr, "Semantic error. ");
      fprintf(stderr, "State '%s' in 'final' isn't contained in 'states'.\n",
          (char *) n->data);
      exit(2);
    }
    n = n->next;
  }
  if (final->states->size == 0) {
    fprintf(stderr, "Semantic error. ");
    fprintf(stderr, "Section 'init' cannot be empty.\n");
    exit(2);
  }
}

static void check_transitions(const struct list *transitions) {
  struct list_node *n = NULL;
  struct list_node *n2 = NULL;
  if ((n = list_find_first_duplicate(transitions, trans_compare)) != NULL) {
    fprintf(stderr, "Semantic error. ");
    fprintf(stderr,
        "Following transition in 'delta' defined more than once.\n");
    fprintf(stderr, "\t%s [ %s ] -> { ",
        ((struct ast_node_transition *) n->data)->from,
        ((struct ast_node_transition *) n->data)->via);
    n2 = ((struct ast_node_transition *) n->data)->to->head;
    while (n2 != NULL) {
      printf("%s ", (char *) (n2->data));
      n2 = n2->next;
    }
    puts("}");
    exit(2);
  }
}

static void check_transition(const struct list *alphabet, const struct list *states,
    const struct ast_node_transition *transition) {
  struct list_node *n = NULL;
  if (!list_contains(states, transition->from, ident_compare)) {
    fprintf(stderr, "Semantic error. ");
    fprintf(stderr, "State '%s' used as /from/ in 'delta' is not contained",
        transition->from);
    fprintf(stderr, " in 'states'.\n");
    exit(2);
  }
  if (transition->via != NULL
      && !list_contains(alphabet, transition->via, ident_compare)) {
    fprintf(stderr, "Semantic error. ");
    fprintf(stderr, "Symbol '%s' used as /via/ in 'delta' is not contained",
        transition->via);
    fprintf(stderr, " in 'alphabet'.\n");
    exit(2);
  }
  if (transition->to->size == 0) {
    fprintf(stderr, "Semantic error. ");
    fprintf(stderr, "Undefined destination in transition in 'delta'.\n");
    fprintf(stderr, "\t%s [ %s ] -> { ??? }\n", transition->from,
        transition->via);
    exit(2);
  }
  if ((n = list_find_first_duplicate(transition->to, ident_compare)) != NULL) {
    fprintf(stderr, "Semantic error. ");
    fprintf(stderr, "Destination state '%s' in 'delta' used more than once.\n",
        (char *) n->data);
    exit(2);
  }
  n = transition->to->head;
  while (n != NULL) {
    if (!list_contains(states, n->data, ident_compare)) {
      fprintf(stderr, "Semantic error. ");
      fprintf(stderr, "State '%s' used as /destination/ in 'delta' is ",
          (char *) n->data);
      fprintf(stderr, "not contained in 'states'.\n");
      exit(2);
    }
    n = n->next;
  }
}
