#ifndef __ast_h__
#define __ast_h__

#include "./list.h"

typedef char * identifier;

struct ast_node_alphabet {
  struct list *symbols;
};

struct ast_node_states {
  struct list *states;
};

struct ast_node_init {
  struct list *states;
};

struct ast_node_final {
  struct list *states;
};

struct ast_node_delta {
  struct list *transitions;
};

struct ast_node_automaton {
  struct ast_node_alphabet alphabet;
  struct ast_node_states states;
  struct ast_node_init init;
  struct ast_node_final final;
  struct ast_node_delta delta;
};

struct ast_node_transition {
  identifier from;
  identifier via;
  unsigned size;
  struct list *to;
};

#endif /* __ast_h__ */
