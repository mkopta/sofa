#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include "./lexan.h"

static FILE *fp;

void lex_init(FILE *istream) {
  fp = istream;
}

int lex_get_next_token(void) {
  int c = 0;
  unsigned state = 0;
  unsigned ident_pos = 0;

  if (fp == NULL) return LEX_ERROR;

AUTOMATON_BEGIN:
  c = fgetc(fp);
  switch(state) {
    case 0:
      /* skip whitespace */
      if (c == ' ' || c == '\t' || c == '\n' || c == '\r') break;
      /* end of input */
      if (c == EOF) return LEX_EOF;
      /* brackets */
      if (c == '{') return LEX_LCB;
      if (c == '[') return LEX_LBB;
      if (c == ']') return LEX_RBB;
      if (c == '}') return LEX_RCB;
      /* arrow '->' */
      if (c == '-') {
        state = 1;
        break;
      }
      /* identifier */
      if (isalnum(c)) {
        memset(lex_ident, '\0', sizeof(char) * LEX_IDENT_SIZE);
        lex_ident[ident_pos++] = (char) c;
        state = 2;
        break;
      }
      /* unknow element */
      return LEX_ERROR;
    case 1:
      /* arrow '->' */
      if (c == '>') return LEX_ARROW;
      else return LEX_ERROR;
    case 2:
      /* identifier */
      if (isalnum(c)) {
        lex_ident[ident_pos++] = (char) c;
        if (ident_pos == LEX_IDENT_SIZE) {
          fprintf(stderr, "Maximum identifier name length reached.\n");
          return LEX_ERROR;
        }
      } else {
        ungetc(c, fp);
        if (strcmp(lex_ident, "alphabet") == 0) {
          return LEX_ALPHABET;
        } else if (strcmp(lex_ident, "states") == 0) {
          return LEX_STATES;
        } else if (strcmp(lex_ident, "init") == 0) {
          return LEX_INIT;
        } else if (strcmp(lex_ident, "final") == 0) {
          return LEX_FINAL;
        } else if (strcmp(lex_ident, "delta") == 0) {
          return LEX_DELTA;
        }
        return LEX_IDENT;
      }
      break;

    default:
      return LEX_ERROR;
  }
  goto AUTOMATON_BEGIN;

  return LEX_ERROR;
}
