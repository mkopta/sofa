#include <stdio.h>
#include <stdlib.h>
#include "./list.h"

struct list *list_create(void) {
  struct list *l = (struct list *) malloc(sizeof(struct list));
  if (l == NULL) {
    perror("malloc");
    exit(3);
  } else {
    l->tail = NULL;
    l->head = NULL;
    l->size = 0;
  }
  return l;
}

void list_destroy(struct list *l) {
  while (!list_empty(l))
    (void) list_pop(l);
  free(l);
}

void *list_pop(struct list *l) {
  void *data = NULL;
  struct list_node *n = l->tail;
  if (l->size == 0) {
    fprintf(stderr, "ERROR: list_pop(), empty list\n");
    exit(3);
  }
  if (n->prev == NULL) {
    l->head = NULL;
    l->tail = NULL;
    l->size = 0;
  } else {
    l->tail = l->tail->prev;
    l->tail->next = NULL;
    l->size--;
  }
  data = n->data;
  free(n);
  return data;
}

void list_push(struct list *l, void *data) {
  struct list_node *n = (struct list_node *) malloc(sizeof(struct list_node));
  if (n == NULL) {
    perror("malloc");
    exit(3);
  }
  n->prev = NULL;
  n->next = NULL;
  n->data = data;
  if (l->size == 0) {
    l->head = n;
    l->tail = n;
    l->size = 1;
  } else {
    l->tail->next = n;
    n->prev = l->tail;
    l->tail = n;
    l->size++;
  }
}

void *list_shift(struct list *l) {
  void *data = NULL;
  struct list_node *n = l->head;
  if (l->size == 0) {
    fprintf(stderr, "ERROR: list_shift(), empty list\n");
    exit(3);
  } else if (l->head->next == NULL) {
    l->head = NULL;
    l->tail = NULL;
    l->size = 0;
  } else {
    l->head = l->head->next;
    l->head->prev = NULL;
    l->size--;
  }
  data = n->data;
  free(n);
  return data;
}

void list_unshift(struct list *l, void *data) {
  struct list_node *n = (struct list_node *) malloc(sizeof(struct list_node));
  if (n == NULL) {
    perror("malloc");
    exit(3);
  }
  n->prev = NULL;
  n->next = NULL;
  n->data = data;
  if (l->size == 0) {
    l->head = n;
    l->tail = n;
    l->size = 1;
  } else {
    n->prev = NULL;
    n->next = l->head;
    l->head->prev = n;
    l->head = n;
    l->size++;
  }
}

unsigned list_insert_list_uniq(const struct list *source, struct list *dest,
    int (*compar)(const void *, const void *)) {
  unsigned inserted = 0;
  struct list_node *n = source->head;
  while (n != NULL) {
    if (!list_contains(dest, n->data, compar)) {
      list_push(dest, n->data);
      inserted++;
    }
    n = n->next;
  }
  return inserted;
}

unsigned list_size(const struct list *const l) {
  return l->size;
}

int list_empty(const struct list *const l) {
  return l->size == 0;
}

int list_contains(const struct list *const l, const void *data,
    int (*compar)(const void *, const void *)) {
  return list_find(l, data, compar) != NULL;
}

struct list_node *list_find(const struct list *l, const void *data,
    int(*compar)(const void *, const void *)) {
  struct list_node *n = l->head;
  while (n != NULL) {
    if (compar(n->data, data) == 0)
      return n;
    n = n->next;
  }
  return NULL;
}

struct list_node *list_find_first_duplicate(const struct list *l,
    int (*compar)(const void *, const void *)) {
  struct list_node *n1 = l->head;
  struct list_node *n2 = l->head;
  while (n1 != NULL) {
    while (n2 != NULL) {
      if (n1 == n2) {
        n2 = n2->next;
        continue;
      }
      if (compar(n1->data, n2->data) == 0)
        return n2;
      n2 = n2->next;
    }
    n1 = n1->next;
    n2 = l->head;
  }
  return NULL;
}

struct list_node *list_find_first_common(const struct list *a,
    const struct list *b, int (*compar)(const void *, const void *)) {
  struct list_node *na = a->head;
  struct list_node *nb = b->head;
  while (na != NULL) {
    while (nb != NULL) {
      if (compar(na->data, nb->data) == 0)
        return nb;
      nb = nb->next;
    }
    nb = b->head;
    na = na->next;
  }
  return NULL;
}

void list_purge(struct list *l) {
  while (!list_empty(l))
    list_pop(l);
}
