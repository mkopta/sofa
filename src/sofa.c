#include <stdio.h>
#include "./ast.h"
#include "./parser.h"
#include "./simulator.h"

static const char *version = "sofa version 1.1";

int main(int argc, char **argv) {
  int result;
  struct ast_node_automaton automaton;
  FILE *fp;

  if (argc < 2) {
    fp = stdin;
  } else if (argc == 2) {
    fp = fopen(*++argv, "r");
    if (fp == NULL) {
      perror("fopen");
      return 1;
    }
  } else {
    fprintf(stderr, "Usage: %s [<inputfile>]\n", *argv);
    return 1;
  }

  parser_init(fp);
  automaton = parser_parse_automaton();
  if (fp != stdin) (void) fclose(fp);
  result = simulate(automaton);
  puts(result ? "NO" : "YES");
  return result;
}
