#ifndef __simulator_h__
#define __simulator_h__

int simulate(const struct ast_node_automaton automaton);

#endif /* __simulator_h__ */
