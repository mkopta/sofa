/* Copyright (c) 2010, Martin Kopta <martin@kopta.eu>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef __list_h__
#define __list_h__

struct list_node {
  void *data;
  struct list_node *prev;
  struct list_node *next;
};

struct list {
  struct list_node *head;
  struct list_node *tail;
  unsigned size;
};

struct list *list_create(void);
void list_destroy(struct list *l);

void *list_pop(struct list *l);
void list_push(struct list *l, void *data);
void *list_shift(struct list *l);
void list_unshift(struct list *l, void *data);

unsigned list_insert_list_uniq(const struct list *source, struct list *dest, int
    (*compar)(const void *, const void *));

unsigned list_size(const struct list *const l);
int list_empty(const struct list *const l);

int list_contains(const struct list *const l, const void *data,
    int (*compar)(const void *, const void *));

struct list_node *list_find(const struct list *l, const void *data,
    int (*compar)(const void *, const void *));
struct list_node *list_find_first_duplicate(const struct list *l,
    int (*compar)(const void *, const void *));
struct list_node *list_find_first_common(const struct list *a,
    const struct list *b, int (*compar)(const void *, const void *));

void list_purge(struct list *l);

#endif /* __list_h__ */
