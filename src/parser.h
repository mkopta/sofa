#ifndef __parser_h__
#define __parser_h__

#include "./ast.h"

void parser_init(FILE *fp);
struct ast_node_automaton parser_parse_automaton(void);

#endif /* __parser_h__ */
