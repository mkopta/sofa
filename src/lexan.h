#ifndef __lexan_h__
#define __lexan_h__

#define LEX_IDENT_SIZE 2048

char lex_ident[LEX_IDENT_SIZE];

void lex_init(FILE *istream);
int lex_get_next_token(void);

enum lex_token {
  LEX_ALPHABET,
  LEX_STATES,
  LEX_INIT,
  LEX_FINAL,
  LEX_DELTA,
  LEX_LCB, /* left curly bracket */
  LEX_RCB, /* right curly bracket */
  LEX_LBB, /* left box bracket */
  LEX_RBB, /* right box bracket */
  LEX_IDENT,
  LEX_ARROW,
  LEX_ERROR,
  LEX_EOF
};

#endif /* __lexan_h__ */
