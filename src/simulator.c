#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "./ast.h"
#include "./list.h"
#include "./simulator.h"

#define MAX_SYMBOL_SIZE 2048

static int ident_compare(const void *a, const void *b) {
  return strcmp((char *) a, (char *) b);
}

static void print_identifiers(const struct list *idents, const char *msg) {
  struct list_node *iterator = NULL;
  printf("\t%s: ", msg);
  iterator = idents->head;
  while (iterator != NULL) {
    printf("%s ", (char *) iterator->data);
    iterator = iterator->next;
  }
  putchar('\n');
}

static void print_delta(const struct list *transitions) {
  struct list_node *iterator = NULL;
  struct list_node *iterator2 = NULL;
  printf("\tDelta:\n");
  iterator = transitions->head;
  while (iterator != NULL) {
    printf("\t\t%s [ ", ((struct ast_node_transition *) (iterator->data))->from);
    printf("%s ] -> { ",
        ((struct ast_node_transition *) (iterator->data))->via == NULL ?
        "" : ((struct ast_node_transition *) (iterator->data))->via);
    iterator2 = ((struct ast_node_transition *) (iterator->data))->to->head;
    while (iterator2 != NULL) {
      printf("%s ", (char *) (iterator2->data));
      iterator2 = iterator2->next;
    }
    puts("}");
    iterator = iterator->next;
  }
}

static void print_automaton(const struct ast_node_automaton a) {
  print_identifiers(a.alphabet.symbols, "Alphabet");
  print_identifiers(a.states.states, "States");
  print_identifiers(a.init.states, "Init");
  print_identifiers(a.final.states, "Final");
  print_delta(a.delta.transitions);
}

static void print_current_state(const struct list *current_state) {
  struct list_node *n;
  printf("Current state: ");
  n = current_state->head;
  while (n != NULL) {
    printf("%s ", (char *) n->data);
    n = n->next;
  }
  putchar('\n');
}


/* Normaly, this algorithm should be repeated while 'state' changes, but because
 * 'state' is linked-list and adding new states is done as appending, actual
 * iterator is BEFORE those added new states and eventualy will get to them, so
 * there is no need to do this algorithm in cycle. All reachable state via
 * epsilon are added incrementaly (looks like unrolling the red carpet) . */
static void include_epsilon_transitions(struct list *state, const struct list
    *transitions) {
  struct list_node *tn = NULL;
  struct list_node *sn = NULL;

  tn = transitions->head;
  sn = state->head;
  while (sn != NULL) {
    while (tn != NULL) {
      if (strcmp(((struct ast_node_transition *) tn->data)->from, (char *)
            sn->data) == 0 && ((struct ast_node_transition *) tn->data)->via ==
          NULL) {
        (void) list_insert_list_uniq(((struct ast_node_transition *)
              tn->data)->to, state, ident_compare);
      }
      tn = tn->next;
    }
    tn = transitions->head;
    sn = sn->next;
  }
}

static struct list *init_current_state(const struct list *init_states, const struct
    list *transitions) {
  struct list *current_state = list_create();
  struct list_node *in = init_states->head;
  while (in != NULL) {
    list_push(current_state, in->data);
    in = in->next;
  }
  include_epsilon_transitions(current_state, transitions);
  return current_state;
}

static void print_automaton_alphabet(const struct list *symbols) {
  struct list_node *n = symbols->head;
  printf("Alphabet: ");
  while (n != NULL) {
    printf("%s ", (char *) n->data);
    n = n->next;
  }
  putchar('\n');
}

static struct list *do_transition(const struct list *transitions, struct list *current_state,
    const char *symbol) {
  struct list *new_state = list_create();
  struct list_node *tn = transitions->head;
  struct list_node *cn = current_state->head;

  while (cn != NULL) {
    while (tn != NULL) {
      if (strcmp(((struct ast_node_transition *) tn->data)->from, (char *)
            cn->data) == 0 && ((struct ast_node_transition *) tn->data)->via !=
          NULL && strcmp(((struct ast_node_transition *) tn->data)->via, symbol)
          == 0) {
        (void) list_insert_list_uniq(((struct ast_node_transition *)
              tn->data)->to, new_state, ident_compare);
      }
      tn = tn->next;
    }
    tn = transitions->head;
    cn = cn->next;
  }
  list_destroy(current_state);
  include_epsilon_transitions(new_state, transitions);
  return new_state;
}

int simulate(const struct ast_node_automaton automaton) {
  struct list_node *n;
  struct list *current_state = init_current_state(automaton.init.states,
      automaton.delta.transitions);
  char *symbol = (char *) malloc(sizeof(char) * MAX_SYMBOL_SIZE);
  int c, i;
  puts("Sofa ready. Insert symbols from alphabet of given automaton "
      "or enter/eof for exit");
  puts("Simulated automaton:");
  print_automaton(automaton);
PROMPT:
  memset(symbol, '\0', sizeof(char) * MAX_SYMBOL_SIZE);
  i = 0;
  print_current_state(current_state);
  printf("Enter next symbol: ");
  fflush(stdout);
  while (isalnum(c = getchar())) {
    symbol[i++] = (char) c;
  }
  if (c == EOF) {
    putchar('\n');
    goto END;
  }
  if (i == MAX_SYMBOL_SIZE) {
    fprintf(stderr, "Error. Maximum input length reached.\n");
  }
  symbol[i] = '\0';
  if (strlen(symbol) == 0) {
    goto END;
  }
  n = automaton.alphabet.symbols->head;
  while (n != NULL) {
    if (strcmp((char *) n->data, symbol) == 0)
      break;
    n = n->next;
  }
  if (n == NULL) {
    fprintf(stderr,
        "Input symbol '%s' isn't contained in automaton alphabet.\n", symbol);
    print_automaton_alphabet(automaton.alphabet.symbols);
  } else {
    current_state = do_transition(automaton.delta.transitions, current_state, symbol);
  }
  goto PROMPT;
END:
  print_current_state(current_state);
  n = current_state->head;
  while (n != NULL) {
    if (list_contains(automaton.final.states, n->data, ident_compare)) {
      /* automaton accepts */
      return 0;
    }
    n = n->next;
  }
  /* automaton does not accept */
  return 1;
}
